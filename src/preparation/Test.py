from MNR import *
from PR import *
import sys
import codecs
import datetime as dt
#sys.path.append("/Users/anaghakaranam/Desktop/Opinion_Spam/coding-playground/data/raw")
metadata_file = "/Users/anaghakaranam/Desktop/Opinion_Spam/coding-playground/data/raw/metadata"




#reading metadata raw file
N = 3000
reviewId, userId, prodId, date, ratings, recommend = [],[],[],[],[],[]
reviewId0 = 1

with codecs.open(metadata_file, 'r', 'utf-8') as fr:
        for line in fr:
            if reviewId0 <= N:
                userId0, prodId0, rating0, recommend0, date0 = line.rstrip().split('\t')
                reviewId.append(reviewId0)
                userId.append(userId0)
                prodId.append(prodId0)
                date.append(date0)
                ratings.append(rating0)
                recommend.append(recommend0)
                reviewId0 += 1


# we have reviewId, userId, prodId, date, rating, recommend
#convert date from string to required format
ratings = np.array(ratings, dtype=np.float32)
recommend = np.array(recommend, dtype=np.float32)

dateformat='%Y-%m-%d'
date = [dt.datetime.strptime(d, dateformat) for d in date]

#example metadata
#5044	0	1.0	-1	2014-11-16
#5045	0	1.0	-1	2014-09-08


print(MNR(prodId,date))
print("\n ###################")
x = "PR"
PR_user = PR_NR(userId, ratings,x)
print(PR_user)