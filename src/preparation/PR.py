#finds the ratio of positive reviews. That is, reviews rated 4 & 5 stars.

# Inputs:
# a)can be users or products
# b)ratings given to the reviews

#Output:
# ratios of positive reviews per user/per product.


import numpy as np



def PR_NR(user_prod, ratings, pn):
    print(type(pn))

    unique_prus, index_prus = np.unique(user_prod, return_inverse=True)

    #we use histograms to group reviews per product or user
    #yet to try with groupby..find an easier way....

    #first make bins for histogra,
    bins = np.arange(len(unique_prus))
    print("bins are: ",bins)
    hist, bin_edges = np.histogram(index_prus, bins=bins)

    print(hist.shape)
    print("bin edges:",bin_edges.shape)

    if(pn == 'PR'):
        print("inside if")
        pos_hist, pos_edges = np.histogram(index_prus[ratings>3], bins=bins)
        print("positive hist",pos_hist.shape)
        return pos_hist / hist

    elif(pn == 'NR'):
        print("inside else")
        neg_hist, neg_edges = np.histogram(index_prus[ratings < 3], bins=bins)
        return neg_hist / hist
