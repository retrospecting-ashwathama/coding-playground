# return [userFeatures, prodFeatures, reviewFeatures, adjlist, label]


import sys
import codecs
import datetime as dt
import numpy as np
from scipy import sparse
import re
import string
import pandas as pd
from nltk import word_tokenize
import csv
from MNR import *
from PR_NR import *
from Feature_suspiciousness_pole import *
from priors import *
from Feature_suspiciousness_pole import *
class Node():

    def __init__(self, v, w):
        self.v = v
        self.w = w

def read_files():
    metadata_file = "/Users/anaghakaranam/Desktop/Opinion_Spam/coding-playground/data/raw/metadata"

    user_features = "user_features.csv"
    try:
        file = open(user_features, 'r')
    except IOError:
        file = open(user_features, 'w')

    N = 2000
    reviewId, userId, prodId, date, ratings, recommend = [], [], [], [], [], []
    reviewId0 = 1

    with codecs.open(metadata_file, 'r', 'utf-8') as fr:
        for line in fr:
            if reviewId0 <= N:
                userId0, prodId0, rating0, recommend0, date0 = line.rstrip().split('\t')
                reviewId.append(reviewId0)
                userId.append(userId0)
                prodId.append(prodId0)
                date.append(date0)
                ratings.append(rating0)
                recommend.append(recommend0)
                reviewId0 += 1

    # we have reviewId, userId, prodId, date, rating, recommend
    # convert date from string to required format
    ratings = np.array(ratings, dtype=np.float32)
    recommend = np.array(recommend, dtype=np.float32)

    dateformat = '%Y-%m-%d'
    date = [dt.datetime.strptime(d, dateformat) for d in date]

    # example metadata
    # 5044	0	1.0	-1	2014-11-16
    # 5045	0	1.0	-1	2014-09-08
    return userId,prodId,date,ratings,recommend

def make_user_features(userId,date,ratings,recommend):
    user_features = pd.DataFrame()

    unique_user = list(np.unique(userId))
    user_features.insert(0, "userId",unique_user)
    #MNR
    user_features.insert(1,"mnr", MNR(userId,date))
    #PR
    user_features.insert(2,"PR", PR_NR(userId,ratings ,"PR"))
    #NR
    user_features.insert(3, "NR",PR_NR(userId,ratings,"NR"))

    print("done with user_features")

    return user_features



def make_prod_features(prodId,date,ratings,recommend):
    unique_prod = list(np.unique(prodId))

    prod_features = pd.DataFrame()


    prod_features.insert(0,"prodId",unique_prod)

    #MNR
    prod_features.insert(1,"mnr",MNR(prodId,date))

    #PR
    prod_features.insert(2,"PR",PR_NR(prodId,ratings,"PR"))

    #NR
    prod_features.insert(2, "NR", PR_NR(prodId, ratings, "NR"))

    print("done with prod features")

    return prod_features


def make_review_features():
    print("waiting for sandy to give 2 features")


#doubt over here..
def make_adj_matrix(userId,prodId,ratings):
    unique_user, index_user = np.unique(userId, return_inverse=True)
    unique_prod, index_prod = np.unique(prodId, return_inverse=True)

    adjlist = [unique_user, unique_prod, ratings]

    print("till here")
    return adjlist







userId,prodId,date,ratings,recommend= read_files()
user_features = make_user_features(userId,date,ratings,recommend)
print(type(user_features))

#print(user_features.loc[:,1])
#prior(user_features,isHighUser)
#print("till here")
#product_features = make_prod_features(prodId,date,ratings,recommend)
#review_features = make_review_features()        #to-do tomorrow.
#make_adj_matrix(userId,prodId,ratings)