%Author: Shebuti Rayana

% for speed up run the individual feature extraction code parallely

%output
%userFeatures : each column is a user feature
%prodFeatures: each column is a product feature
%reviewFeatures: each column is a review feature
%adjlist: adjacency list of the review network
%label: label of the reviews (if exist)

function [userFeatures, prodFeatures, reviewFeatures, adjlist, label] = featureExtraction(file1, file2)
    fid = fopen(file1);
    C = textscan(fid, '%s %s %s %s %s');
    %review_id = C{1};
    user_id = C{1};
    prod_id = C{2};
    rating = str2double(C{3});
    label = str2double(C{4});
    date = cellstr(datestr(datevec(C{5}),'dd-mmm-yyyy'));
    clear C;
    fclose(fid);
    
    dateFormat = 'dd-mmm-yyyy';
    
    % adjacency list of the review network
    [~,userID] = ismember(user_id,unique(user_id));
    [~,prodID] = ismember(prod_id,unique(prod_id));
    adjlist = [userID,prodID,rating];
    
    userFeatures = [];
    prodFeatures = [];
    reviewFeatures = [];
    %% user and product features
    
    % MNR feature for both user and product
    [MNR_user,MNR_prod] = MNR(user_id, prod_id, date, dateFormat);
    userFeatures = [userFeatures,double(MNR_user)];
    prodFeatures = [prodFeatures,double(MNR_prod)];

    % PR feature for both user and product
    [PR_user,PR_prod] = PR(user_id,prod_id,rating);
    userFeatures = [userFeatures,double(PR_user)];
    prodFeatures = [prodFeatures,double(PR_prod)];

    % NR feature for both user and product
    [NR_user,NR_prod] = NR(user_id,prod_id,rating);
    userFeatures = [userFeatures,double(NR_user)];
    prodFeatures = [prodFeatures,double(NR_prod)];

    % avgRD feature for both user and product
    [avgRD_user,avgRD_prod] = avgRD(user_id, prod_id, rating);  
    userFeatures = [userFeatures,double(avgRD_user)];
    prodFeatures = [prodFeatures,double(avgRD_prod)];
    
    % WRD feature for both user and product
    [WRD_user,WRD_prod] = WRD(user_id, prod_id, rating, date, dateFormat);
    userFeatures = [userFeatures,double(WRD_user)];
    prodFeatures = [prodFeatures,double(WRD_prod)];

    % ERD feature for both user and product
    [ERD_user,ERD_prod] = ERD(user_id, prod_id, rating);
    userFeatures = [userFeatures,double(ERD_user)];
    prodFeatures = [prodFeatures,double(ERD_prod)];

    % BST feature for user 
    BST_user = BST(user_id, date, dateFormat);
    userFeatures = [userFeatures,double(BST_user)];

    % ETG feature for both user and product
    [ETG_user, ETG_prod] = ETG(user_id, prod_id, date, dateFormat);
    userFeatures = [userFeatures,double(ETG_user)];
    prodFeatures = [prodFeatures,double(ETG_prod)];
    
    % RL feature for both user and product
    
    % calling python script WordCount_reviews.py for counting words in
    % review text and save in output_wordcount.txt (change the python path according to your settings)
    % here, emoticons.txt is a dictionary of emoticons
    
    !C:\Python27\python.exe WordCount_reviews.py file2 output_wordcount.txt
    
    filename = 'output_wordcount.txt';
    fid = fopen(filename);
    C = textscan(fid, '%s %s','Delimiter', ' ');
    wordcount = str2double(C{2});
    clear C;
    fclose(fid);

    [RL_user,RL_prod] = RL(user_id,prod_id,wordcount);
    userFeatures = [userFeatures,double(RL_user)];
    prodFeatures = [prodFeatures,double(RL_prod)];
    
    % extract bigrams from reviews
    % run bigram.py
    % suggestion: run the python codes separately
    
    !C:\Python27\python.exe biGram.py file2 output_biGram.txt
    
    % calculate TFIDF of reviews for bigrams
    [TFIDF,binaryBG,uniqueRID] = TFIDF_biGram(review_id,'output_biGram.txt');
    
    save('TFIDF','TFIDF','-v7.3');
    
    % ACS and MCS features for users
    [ACS_user, MCS_user] = ACS_MCS_user(user_id,TFIDF);
    userFeatures = [userFeatures,double(ACS_user)];
    userFeatures = [userFeatures,double(MCS_user)];
    
    % ACS and MCS features for products
    [ACS_prod, MCS_prod] = ACS_MCS_prod(prod_id,TFIDF);
    prodFeatures = [prodFeatures,double(ACS_prod)];
    prodFeatures = [prodFeatures,double(MCS_prod)];

    %% review features
    %  Rank feature for reviews
    rank = Rank(prod_id,date, dateFormat);
    reviewFeatures = [reviewFeatures,double(rank)];

    % RD feature for reviews
    RD_reviews = RD(prod_id, rating);
    reviewFeatures = [reviewFeatures,double(RD_reviews)];

    % EXT feature for reviews
    EXT_reviews = EXT(rating);
    reviewFeatures = [reviewFeatures,double(EXT_reviews)];
    
    % DEV feature for reviews
    DEV_reviews = DEV(prod_id, rating);
    reviewFeatures = [reviewFeatures,double(DEV_reviews)];
    
    % ETF feature for reviews
    ETF_reviews = ETF(user_id, prod_id, date , dateFormat);
    reviewFeatures = [reviewFeatures,double(ETF_reviews)];

    % ISR feature for reviews
    ISR_reviews = ISR(user_id);
    reviewFeatures = [reviewFeatures,double(ISR_reviews)];

    % review text feature extraction
    
    % PCW feature for reviews
    
    !C:\Python27\python.exe allCapitalCount.py file2 output_AllCapital.csv
    
    filename = 'output_AllCapital.csv';
    fid = fopen(filename);
    C = textscan(fid, '%s %f', 'Delimiter', ',');
    PCW_reviews = C{2};
    clear C;
    fclose(fid);
    
    reviewFeatures = [reviewFeatures,double(PCW_reviews)];
    
    % PC feature for reviews
    
    !C:\Python27\python.exe countCapital.py file2 output_PC.csv
    
    filename = 'output_PC.csv';
    fid = fopen(filename);
    C = textscan(fid, '%s %f', 'Delimiter', ',');
    PC_reviews = C{2};
    clear C;
    fclose(fid);
    
    reviewFeatures = [reviewFeatures,double(PC_reviews)];
    
    % L feature for reviews
    
    L_reviews = wordcount./max(wordcount);
    
    reviewFeatures = [reviewFeatures,double(L_reviews)];
    
    % PP1 feature for reviews
    
    !C:\Python27\python.exe ratioPPwordCount.py file2 output_PP1.csv
    
    filename = 'output_PP1.csv';
    fid = fopen(filename);
    C = textscan(fid, '%s %f %f', 'Delimiter', ',');
    PP1_reviews = C{2};
    clear C;
    fclose(fid);
    
    reviewFeatures = [reviewFeatures,double(PP1_reviews)];
    
    % RES feature for reviews
    
    !C:\Python27\python.exe excSentenceCount.py file2 output_RES.csv
    
    filename = 'output_RES.csv';
    fid = fopen(filename);
    C = textscan(fid, '%s %f', 'Delimiter', ',');
    RES_reviews = C{2};
    clear C;
    fclose(fid);
    
    reviewFeatures = [reviewFeatures,double(RES_reviews)];
    
    
    % SW and OW features for reviews
    
    !C:\Python27\python.exe sentimentAnalysis.py file2 output_SW+OW.csv
    
    filename = 'output_SW+OW.csv';
    fid = fopen(filename);
    C = textscan(fid, '%s %f %f', 'Delimiter', ',');
    SW_reviews = C{3};
    OW_reviews = C{2};
    clear C;
    fclose(fid);
    
    reviewFeatures = [reviewFeatures,double(SW_reviews),double(OW_reviews)];
    
    % F feature for reviews
    r = 20;
    b = 50;
    candidategroups = jac_doc_hash(binaryBG,r,b); % LSH
    
    F_reviews = zeros(length(review_id),1);

    for i = 1:length(review_id)
        [loca,locb] = ismember(review_id(i),uniqueRID);
        if(loca)
            F_reviews(i) = sum(candidategroups == candidategroups(locb));
        else
            F_reviews(i) = 1;
        end
    end

    reviewFeatures = [reviewFeatures,double(F_reviews)];
    
    % DL_u and DL_b features
    
    !C:\Python27\python.exe uniGram.py file2 output_uniGram.txt
    
    !C:\Python27\python.exe codeTable.py output_uniGram.txt output_DL_u.csv dict_uniGram.csv
    
    DL_u = zeros(length(review_id),1);
    filename = 'uniGram_DL_u.csv';
    fid = fopen(filename);
    C = textscan(fid, '%s %f','Delimiter', ',');
    temp = C{2};
    [loc,ind] = ismember(review_id,C{1});
    ind(ind == 0) = [];
    DL_u(loc == 1) = temp(ind);
    clear C;
    fclose(fid);
    
    reviewFeatures = [reviewFeatures,double(DL_u)];
    
    !C:\Python27\python.exe codeTable.py output_biGram.txt output_DL_b.csv dict_biGram.csv
    
    DL_b = zeros(length(review_id),1);
    filename = 'uniGram_DL_b.csv';
    fid = fopen(filename);
    C = textscan(fid, '%s %f','Delimiter', ',');
    temp = C{2};
    [loc,ind] = ismember(review_id,C{1});
    ind(ind == 0) = [];
    DL_b(loc == 1) = temp(ind);
    clear C;
    fclose(fid);
    
    reviewFeatures = [reviewFeatures,double(DL_b)];
     
end