%Author: Shebuti Rayana
% Calculating priors: combining all features with different scalling to a
% single number as prior

%input:
%features: Each column contains a feature vector
%featurePoles: 1 means high is suspicious, 0 means low is suspicious

%output:
%prior: prior calculated from F features (user features, product features, review features)

function [prior] = priors(features, featurePoles)
    n = size(features,1);
    numfeatures = size(features,2);
    comp = ones(n,numfeatures)*0.5;
    
    for k = 1:numfeatures
        f = features(:,k);
        [sf,~] = sort(f, 'ascend');

        for i = 1:n    
            if(f(i) == -1) % prior remains unbiased
                continue;
            end
            ind = find(sf==f(i));

            if(featurePoles(k)) 
                NCDF = ind(1) / n;
                comp(i,k) = 1 - NCDF; % if high is suspicious
            else
                NCDF = ind(end) / n;
                comp(i,k) = NCDF; % if low is suspicious
            end   
        end
    end

    prior = 1 - sqrt(sum(comp.^2,2)/numfeatures);

    prior(prior==1) = 0.999;
    prior(prior==0) = 0.001;

end