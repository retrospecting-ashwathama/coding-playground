% calculating adjacency list for review-review (for similar reviews if cosine similarity is >= 0.9)

function reviewAdjMat = simsim_edge(TFIDF,r,b)

binaryBG = TFIDF;
binaryBG(binaryBG > 0) = 1;

c = jac_doc_hash(binaryBG,r,b);

N = size(TFIDF,2);

uC = unique(c);

reviewAdjMat = [];

for i = 1:length(uC)
    index = find(c == uC(i));
    if(length(index) > 1)
        T = TFIDF(:,index);
        P = 1:size(T,2);
        pairs = nchoosek(P,2);
        for j = 1:size(pairs,1)
            x = T(:,pairs(j,1));
            y = T(:,pairs(j,2));
            Sim = dot(x,y)/(norm(x,2)*norm(y,2));
            if(Sim >= 0.9) % for finding reviews with similarity >= 0.9
                in = [index(pairs(j,1)),index(pairs(j,2))];
                reviewAdjMat = [reviewAdjMat;in];
            end
        end
    end
end

size(reviewAdjMat)

end