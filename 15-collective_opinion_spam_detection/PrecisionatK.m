% Author: Shebuti Rayana

function pre = PrecisionatK(RankList,label,K)
    gt = find(label == -1);
    a = RankList(1:K);
    dum = ismember(gt,a);
    TP = sum(dum);
    FP = length(a) - TP;
    pre = TP/(TP+FP);
end