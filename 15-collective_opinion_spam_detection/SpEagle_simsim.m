function [beliefs1, beliefs2, beliefs3] = SpEagle_simsim( adjlist, adjlist2, nodep1, nodep2, reviewp, edgep, it )

% adjlist : (Ex3) adjacency list; 
% First two columns for source (1...N) and dest (1...M), N: #users and M: #prods
% 3rd column either (1) 1 for + and 2 for - , or (2) 1-5: ratings
% nodep1: NxK1 matrix of initial prior potentials; N: #users, K1: classes/states (benign,spammer)
% nodep2: MxK2 matrix of initial prior potentials; M: #prod.s, K2: classes/states (non-targeted,targeted)
% reviewp: ExK1 matrix of initial prior potentials; E: #reviews, K1: classes/states (genuine,fake)
% edgep: K1xK2x|2|, 1: for review x product relations, 2: for user x review relations (homophily)
% uses edgep.mat 
% edgep(:,:,1) = 0.9000    0.1000      0.1000    0.9000  
% edgep(:,:,2) = 1         0           0           1
% edgep(:,:,3) = 0.9000    0.1000      0.1000    0.9000
% it: max #iterations
% adjlist2: review X review adjlist depending on their similarity >= 0.9

edgep

[N K1] = size(nodep1);
[M K2] = size(nodep2);
E = size(adjlist,1);
E2 = size(adjlist2,1);

% initializing all messages to 1
% holding ALL msgs in log's so init to 0

% review --- product
m_to = zeros(E,K1);
m_from = zeros(E,K1);

% user --- review
m2_to = zeros(E,K1);
m2_from = zeros(E,K1);

% review --- review
ms_to = zeros(E2,K1);
ms_from = zeros(E2,K1);

nodep1 = log(nodep1);
nodep2 = log(nodep2);
reviewp = log(reviewp);
edgep = log(edgep);

epsilon = 10^-6;

% for each node 
repeat = true;
iter=0;
ts=tic;
while( repeat )    
    tic
    iter=iter+1
	maxdiff = -Inf;
	
	% compute product buffer for N
	prodN = zeros(N,K1);
	for i=1:E
		prodN(adjlist(i,1),:) = prodN(adjlist(i,1),:) + ( m2_from(i,:) );
	end
	
	
    alldiff=zeros(4*E+2*E2,1);

    % compute messages to E review nodes
	for i=1:E
		a = adjlist(i,1);
		
		mn = ( prodN(a,:) - ( m2_from(i,:) ) );
            
		part = nodep1(a,:) + mn;
		newmsg = zeros(1,K1);              
		for k=1:K1
			term = ( part + edgep( :,k,2 )' ); 
			newmsg(k) = log(sum(exp(term-max(term)))) + max(term); 
		end
		
		% normalize: newmsg(i)/sum(newmsg)
		newmsg = newmsg-( log(sum(exp(newmsg-max(newmsg)))) + max(newmsg) );
		
		
		fark = exp(newmsg) - exp(m2_to(i,:));
		diff = norm(fark);		
        alldiff(i) = diff;		
		if( diff > maxdiff)
			maxdiff = diff;  
		end 

		m2_to(i,:) = newmsg; 
	
	end % iterating E reviews
                    
                    
%%%%%%%%%%%%%%% %%%%%%%%%%%%%%% %%%%%%%%%%%%%%% %%%%%%%%%%%%%%% %%%%%%%%%%%%%%% %%%%%%%%%%%%%%%

    % compute review buffer for E
    prodE = zeros(E,K1);
                    
    for i=1:E2
        prodE(adjlist2(i,1),:) = prodE(adjlist2(i,1),:) + ( ms_from(i,:) );
        prodE(adjlist2(i,2),:) = prodE(adjlist2(i,2),:) + ( ms_to(i,:) );
    end   
    
    % compute messages to M product nodes
    for i=1:E
                    
       mn = m2_to(i,:);
        
       if(abs(1/K1-exp(reviewp(i,1)))<=0.05)             
                    part =  mn;
        else
                    part = reviewp(i,:) + mn; % multiply only if prior biased enough
        end
       
       newmsg = zeros(1,K2);              
       for k=1:K2
           term = ( part + edgep( :,k,1 )' ); 
           newmsg(k) = log(sum(exp(term-max(term)))) + max(term); 
       end
                            
       % normalize: newmsg(i)/sum(newmsg)
       newmsg = newmsg-( log(sum(exp(newmsg-max(newmsg)))) + max(newmsg) );
                            
                            
       fark = exp(newmsg) - exp(m_to(i,:));
       diff = norm(fark);		
       alldiff(i+E) = diff;		
       if( diff > maxdiff)
           maxdiff = diff;  
       end 
                            
       m_to(i,:) = newmsg; 
                            
    end % iterating N nodes
 
 %%%%%%%%%%%%%%% %%%%%%%%%%%%%%% %%%%%%%%%%%%%%% %%%%%%%%%%%%%%% %%%%%%%%%%%%%%% %%%%%%%%%%%%%%%                  
		
	% compute product buffer for M
	prodM = zeros(M,K2);
	for i=1:E
		prodM(adjlist(i,2),:) = prodM(adjlist(i,2),:) + ( m_to(i,:) );
	end
                   
    % compute messages to E review nodes
    for i=1:E
        a = adjlist(i,2);

        mn = ( prodM(a,:) - ( m_to(i,:) ) );
                   
        part = nodep2(a,:) + mn;
        newmsg = zeros(1,K1);              
        for k=1:K1
            term = ( part + edgep( k,:,1 ) );  
            newmsg(k) = log(sum(exp(term-max(term)))) + max(term); 
        end
                   
        % normalize: newmsg(i)/sum(newmsg)
        newmsg = newmsg-( log(sum(exp(newmsg-max(newmsg)))) + max(newmsg) );
                   
                   
        fark = exp(newmsg) - exp(m_from(i,:));
        diff = norm(fark);
        alldiff(i+2*E) = diff;	
                   
        if( diff > maxdiff)
            maxdiff = diff;  
        end     
                   
        m_from(i,:) = newmsg; 
                   
    end % iterating E review nodes                  

%%%%%%%%%%%%%%% %%%%%%%%%%%%%%% %%%%%%%%%%%%%%% %%%%%%%%%%%%%%% %%%%%%%%%%%%%%% %%%%%%%%%%%%%%%                   
	
	% compute messages to N nodes
	for i=1:E

		mn = m_from(i,:);
		
        if(abs(1/K1-exp(reviewp(i,1)))<=0.05)             
                   part =  mn;
        else
                   part = reviewp(i,:) + mn;
        end
                   
		newmsg = zeros(1,K1);              
		for k=1:K1
			term = ( part + edgep( k,:,2 ) );  
			newmsg(k) = log(sum(exp(term-max(term)))) + max(term); 
		end

        % normalize: newmsg(i)/sum(newmsg)
		newmsg = newmsg-( log(sum(exp(newmsg-max(newmsg)))) + max(newmsg) );
		
		
		fark = exp(newmsg) - exp(m2_from(i,:));
		diff = norm(fark);
        alldiff(i+3*E) = diff;	
		
		if( diff > maxdiff)
			maxdiff = diff;  
		end     
		
		m2_from(i,:) = newmsg; 
		
	end % iterating N nodes

%%%%%%%%%%%%%%% %%%%%%%%%%%%%%% %%%%%%%%%%%%%%% %%%%%%%%%%%%%%% %%%%%%%%%%%%%%% %%%%%%%%%%%%%%% 

    for i=1:E
         prodE(i,:) = prodE(i,:) + m_from(i,:) + m2_to(i,:);
    end
                   
    % compute messages to E similar reviews
                   
    for i=1:E2
       a = adjlist2(i,1);
       b = adjlist2(i,2);
       mn = ( prodE(a,:) - ( ms_from(i,:) ) );
       mnb = ( prodE(b,:) - ( ms_to(i,:) ) );
                   
       part = reviewp(a,:) + mn;
       newmsg = zeros(1,K1);              
       for k=1:K1
          term = ( part + edgep( :,k, 3 )' ); 
          newmsg(k) = log(sum(exp(term-max(term)))) + max(term); 
       end
                           
       partb = reviewp(b,:) + mnb;
       newmsgb = zeros(1,K1);              
       for k=1:K1
          term = ( partb + edgep( :,k, 3 )' ); 
          newmsgb(k) = log(sum(exp(term-max(term)))) + max(term); 
       end
                                   
       % normalize: newmsg(i)/sum(newmsg)
       newmsg = newmsg-( log(sum(exp(newmsg-max(newmsg)))) + max(newmsg) );
                                   
       fark = exp(newmsg) - exp(ms_to(i,:));
       diff = norm(fark);		
       alldiff(i+4*E) = diff;		
       if( diff > maxdiff)
            maxdiff = diff;  
       end 
                                   
       newmsgb = newmsgb-( log(sum(exp(newmsgb-max(newmsgb)))) + max(newmsgb) );
                                   
       fark = exp(newmsgb) - exp(ms_from(i,:));
       diff = norm(fark);		
       alldiff(i+4*E+E2) = diff;		
       if( diff > maxdiff)
           maxdiff = diff;  
       end 
                                   
       ms_to(i,:) = newmsg; 
                                   
       ms_from(i,:) = newmsgb;
                                   
   end % iterating N nodes

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                   
	%salldiff = sort(alldiff);
	maxd = max(alldiff) 
	mind = min(alldiff) 
	meand = mean(alldiff)
	p90 = quantile(alldiff, 0.9)
	%pause
	
	if maxdiff < epsilon
        repeat = false;
    end
    
    if(iter==it)        
        break;
    end
	toc
	
end
toc(ts)
iter



% compute beliefs
beliefs1 = zeros(N,K1);
beliefs2 = zeros(M,K2);
beliefs3 = zeros(E,K1);
                   
% compute product buffer for N
for i=1:E
	a = adjlist(i,1);
	b = adjlist(i,2);
	beliefs1(a,:) = beliefs1(a,:) + ( m2_from(i,:) );
	beliefs2(b,:) = beliefs2(b,:) + ( m_to(i,:) );	
    beliefs3(i,:) = beliefs3(i,:) + ( m2_to(i,:) ) + ( m_from(i,:) );        
end

for i=1:E2
    a = adjlist2(i,1);
    b = adjlist2(i,2);
    beliefs3(a,:) = beliefs3(a,:) + ( ms_from(i,:) );
    beliefs3(b,:) = beliefs3(b,:) + ( ms_to(i,:) );	
end

for a=1:N
	beliefs1(a,:) = (nodep1(a,:)) + ( beliefs1(a,:) );
	
	nwm = beliefs1(a,:);
	for k=1:K1
		beliefs1(a,k) = 1 / ( sum(exp(nwm-nwm(k))) );
	end
end
for b=1:M
	beliefs2(b,:) = (nodep2(b,:)) + ( beliefs2(b,:) );
	
	nwm = beliefs2(b,:);
	for k=1:K2
		beliefs2(b,k) = 1 / ( sum(exp(nwm-nwm(k))) );
	end
end
for i=1:E
    beliefs3(i,:) = (reviewp(i,:)) + ( beliefs3(i,:) );
                   
    nwm = beliefs3(i,:);
    for k=1:K1
        beliefs3(i,k) = 1 / ( sum(exp(nwm-nwm(k))) );
    end
end                   
                   
end