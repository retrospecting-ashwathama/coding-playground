% Author : Shebuti Rayana(Stony Brook University)
% Email: srayana@cs.stonybrook.edu
% Available for academic use only, not for commercial use

Start Matlab and run:
  (i) runSpEagle for unsupervised SpEagle
  (ii) runSpEaglePlus for semi-supervised SpEaglePlus
  (iii) runSpEagle_simsim for SpEagle with review-review similarity(>=0.9 cosine similarity) edges 
  
 
 Please cite: (if you are using this work)
 
 @inproceedings{DBLP:conf/sigkdd/Akoglu15,
   author    = {Shebuti Rayana and Leman Akoglu},
   title     = {Collective Opinion Spam Detection: Bridging Review Networks and metadata},
   booktitle = {Proceeding of the 21st ACM SIGKDD international conference 
    on Knowledge discovery and data mining, {KDD'15}},
   year      = {2015},
  }
