%Author: Shebuti Rayana

%% calculating the features for users, reviews, products

% following are the two input file containing both meta data and review
% network information

fileName1 = 'metadata.txt'; % contains review_id, user_id, product_id, timestamp of review, rating(1-5*), and 
                            % label(recommended +1 and non-recommended -1)
                            % of reviews
fileName2 = 'reviewText.txt'; % contains review_id and review_text

[userFeatures, prodFeatures, reviewFeatures, adjlist, label] = featureExtraction(fileName1, fileName2);
N = max(adjlist(:,1)); % number of users
M = max(adjlist(:,2)); % number of products
E = size(adjlist,1); % number of reviews

% label users, if a user wrote at least one filtered review s/he is fraud
A = sparse(double(adjlist(:,1)),double(adjlist(:,2)),double(label),double(N),double(M));
[labelUser, ~] = min(A,[],2);

%% calculate prior for users
% for some features higher is more anomalous and for others lower
% MNR - H , PR - H, NR - H, avgRD - H, WRD - H, BST - H, ERD - L, ETG - L, RL - L, ACS - H, MCS - H 

isHighUser = [1 1 1 1 1 1 0 0 0 1 1]'; % according to suspiciousness pole (high vs low) of user 
priorU = priors(userFeatures, isHighUser);

%% calculate prior for products
% for some features higher is more anomalous and for others lower
% MNR - H , PR - H, NR - H, avgRD - H, WRD - H, ERD - L, ETG - L, RL - L, ACS - H, MCS - H 

isHighProd = [1 1 1 1 1 0 0 0 1 1]'; % according to suspiciousness pole (high vs low) of product features
priorP = priors(prodFeatures,isHighProd);

%% calculate prior for reviews
% for some features higher is more anomalous and for others lower
% Rank - L, RD - H, EXT - H, DEV - H, ETF - H, ISR - H, PCW - H, PC - H, 
% L - L, PP1 - L, RES - H, SW - H, OW - L, F - H, DL_u - L, DL_b - L

isHighReview = [0 1 1 1 1 1 1 1 0 0 1 1 0 1 0 0]'; % according to suspiciousness pole (high vs low) of reviews features
priorR = priors(reviewFeatures, isHighReview);

upriors = [1-priorU priorU];
ppriors = [1-priorP priorP];
load('edgep.mat');

T = 10;

nonfiltix = 1:E;
filtix = find(label==-1);
nonfiltix(filtix)=[];
class1 = length(filtix);
class2 = length(nonfiltix);

Rev = length(label);
numlabeled = round((Rev*0.01)/2); % 1 percent labeled (1% - 10%)

topK = [100:100:1000]';

ap1 = zeros(T,1);
ap2 = zeros(T,1);
auc1 = zeros(T,1);
auc2 = zeros(T,1);
pre_user = [];
NDCG_user = [];
pre_review = [];
NDCG_review = [];

for t = 1:T % repeat T times
%%%%%%%%%%%%%%%%%%%%%%%%%%  label subset  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
labeledreviews = zeros(E,1); % if not labeled, 0

while(1)
spamlabeledind = unidrnd(class1,1,2*numlabeled);
ul = unique(spamlabeledind);
    if(length(ul) >= numlabeled)
        spamlabeledind = ul(1:numlabeled);
        break;
    end
end
labeledreviews( filtix(spamlabeledind) ) = -1;

% pick as many non-filtered reviews at random 
while(1)
nonspamlabeledind = unidrnd(class2,1,2*numlabeled);
ul = unique(nonspamlabeledind);
    if(length(ul) >= numlabeled)
        nonspamlabeledind = ul(1:numlabeled);
        break;
    end
end
labeledreviews( nonfiltix(nonspamlabeledind) ) = 1;

rpriors = [1-priorR priorR];
rpriors( filtix(spamlabeledind),: ) = repmat([0.001 .999],numlabeled,1);
rpriors( nonfiltix(nonspamlabeledind),: ) = repmat([0.999 0.001],numlabeled,1);

[beliefsUser, beliefsProd, beliefsReview] = SpEaglePlus( adjlist, upriors, ppriors, rpriors, labeledreviews, edgep, 100 );

% do not evaluate labeled reviews
lind = find(labeledreviews~=0);
label2 = label;
label2(lind) = [];
%ground truth
index = find(labeledreviews == 0);
beliefsReview = beliefsReview(index,:);

% user ranking
[~,R] = sort(beliefsUser(:,2),'descend');
[ap1(t),~,~] = AveragePrecision(R,labelUser,0); 
[auc1(t),~,~] = ROC_AUC(R,labelUser,0);

pre = zeros(length(topK),1);
NDCG = zeros(length(topK),1);
for j = 1:length(topK)
    pre(j) = PrecisionatK(R,labelUser,topK(j));
    NDCG(j) = NDCGatK(R,labelUser,topK(j));
end
pre_user = [pre_user,pre];
NDCG_user = [NDCG_user,NDCG];


% review ranking
[~,R] = sort(beliefsReview(:,2),'descend');
[ap2(t),~,~] = AveragePrecision(R,label2,0);
[auc2(t),~,~] = ROC_AUC(R,label2,0);

pre = zeros(length(topK),1);
NDCG = zeros(length(topK),1);
for j = 1:length(topK)
    pre(j) = PrecisionatK(R,label2,topK(j));
    NDCG(j) = NDCGatK(R,label2,topK(j));
end
pre_review = [pre_review,pre];
NDCG_review = [NDCG_review,NDCG];

end


%% results
filename = 'results_SpEaglePlus.txt';
fileID = fopen(filename,'wt');

%% User Ranking
fprintf('----------User Ranking--------------\n\r');
% averge precision
fprintf(fileID,'Average Precision of SpEagle: %f \n\r',mean(ap1));

% area under curve
fprintf(fileID,'Area under curve of SpEagle: %f \n\r',mean(auc1));

% precision at k and NDCG at k (e.g. k = 100, 200, 300, ... 1000)
for i = 1:length(topK)
    fprintf(fileID,'Precision at %d: %f and NDCG at %d: %f \n\r',topK(i),mean(pre_user(i,:)),topK(i),mean(NDCG_user(i,:)));
end

%% Review Ranking
fprintf('----------Review Ranking--------------\n\r');
% averge precision
fprintf(fileID,'Average Precision of SpEagle: %f \n\r',mean(ap2));

% area under curve
fprintf(fileID,'Area under curve of SpEagle: %f \n\r',mean(auc2));

% precision at k and NDCG at k (e.g. k = 100, 200, 300, ... 1000)

for i = 1:length(topK)
    fprintf(fileID,'Precision at %d: %f and NDCG at %d: %f \n\r',topK(i),mean(pre_review(i,:)),topK(i),mean(NDCG_review(i,:)));
end

    


