%Author: Shebuti Rayana

function NDCG = NDCGatK(RankList,label,K)
    l = zeros(length(label),1);
    l(label == -1) = 1;
    lab = l(RankList);
    
    ci = lab(1:K);
    i = [1:K]';
    DCG = sum((2.^ci - 1)./log2(1+i));
    ci = ones(K,1);
    IDCG = sum((2.^ci - 1)./log2(1+i));
    NDCG = DCG/IDCG;
end