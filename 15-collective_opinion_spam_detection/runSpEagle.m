%Author: Shebuti Rayana

%% calculating the features for users, reviews, products

% following are the two input file containing both meta data and review
% network information


fileName1 = 'metadata'; % contains user_id, product_id, rating(1-5*), 
                            % label(recommended +1 and non-recommended -1),
                            % and date of reviews
fileName2 = 'reviewContent'; % contains review_text

[userFeatures, prodFeatures, reviewFeatures, adjlist, label] = featureExtraction(fileName1, fileName2);
N = max(adjlist(:,1)); % number of users
M = max(adjlist(:,2)); % number of products
E = size(adjlist,1); % number of reviews

%% calculate prior for users
% for some features higher is more anomalous and for others lower
% MNR - H , PR - H, NR - H, avgRD - H, WRD - H, BST - H, ERD - L, ETG - L, RL - L, ACS - H, MCS - H 

isHighUser = [1 1 1 1 1 1 0 0 0 1 1]'; % according to suspiciousness pole (high vs low) of user 
priorU = priors(userFeatures, isHighUser);

%% calculate prior for products
% for some features higher is more anomalous and for others lower
% MNR - H , PR - H, NR - H, avgRD - H, WRD - H, ERD - L, ETG - L, RL - L, ACS - H, MCS - H 

isHighProd = [1 1 1 1 1 0 0 0 1 1]'; % according to suspiciousness pole (high vs low) of product features
priorP = priors(prodFeatures,isHighProd);

%% calculate prior for reviews
% for some features higher is more anomalous and for others lower
% Rank - L, RD - H, EXT - H, DEV - H, ETF - H, ISR - H, PCW - H, PC - H, 
% L - L, PP1 - L, RES - H, SW - H, OW - L, F - H, DL_u - L, DL_b - L

isHighReview = [0 1 1 1 1 1 1 1 0 0 1 1 0 1 0 0]'; % according to suspiciousness pole (high vs low) of reviews features
priorR = priors(reviewFeatures, isHighReview);

upriors = [1-priorU priorU];
ppriors = [1-priorP priorP];
rpriors = [1-priorR priorR];
load('edgep.mat');

[beliefsUser, beliefsProd, beliefsReview] = SpEagle(adjlist, upriors, ppriors, rpriors, edgep, 100); % unsupervised

%% evaluation - done only on users and reviews
% label users, if a user wrote at least one filtered review s/he is fraud
A = sparse(double(adjlist(:,1)),double(adjlist(:,2)),double(label),double(N),double(M));
[labelUser, ~] = min(A,[],2);

filename = 'results_SpEagle.txt';
fileID = fopen(filename,'wt');
    %% User Ranking
    fprintf('----------User Ranking--------------\n\r');
    [~,R] = sort(beliefsUser(:,2),'descend');
    % averge precision
    [AP,~,~] = AveragePrecision(R,labelUser,0);
    fprintf(fileID,'Average Precision of SpEagle: %f \n\r',AP);
    
    % area under curve
    [AUC,~,~] = ROC_AUC(R,labelUser,0);
    fprintf(fileID,'Area under curve of SpEagle: %f \n\r',AUC);

    % precision at k and NDCG at k (e.g. k = 100, 200, 300, ... 1000)
    k = [100:100:1000]';
    
    for i = 1:length(k)
        precisionATk = PrecisionatK(R,labelUser,k(i));
        NDCGATk = NDCGatK(R,labelUser,k(i));
        fprintf(fileID,'Precision at %d: %f and NDCG at %d: %f \n\r',k(i),precisionATk,k(i),NDCGATk);
    end
    
    %% Review Ranking
    fprintf('----------Review Ranking--------------\n\r');
    [~,R] = sort(beliefsReview(:,2),'descend');
    % averge precision
    [AP,~,~] = AveragePrecision(R,label,0);
    fprintf(fileID,'Average Precision of SpEagle: %f \n\r',AP);
    
    % area under curve
    [AUC,~,~] = ROC_AUC(R,label,0);
    fprintf(fileID,'Area under curve of SpEagle: %f \n\r',AUC);

    % precision at k and NDCG at k (e.g. k = 100, 200, 300, ... 1000)
    k = [100:100:1000]';
    
    for i = 1:length(k)
        precisionATk = PrecisionatK(R,label,k(i));
        NDCGATk = NDCGatK(R,label,k(i));
        fprintf(fileID,'Precision at %d: %f and NDCG at %d: %f \n\r',k(i),precisionATk,k(i),NDCGATk);
    end
    


